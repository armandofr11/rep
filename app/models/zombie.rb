class Zombie < ApplicationRecord
		has_many :brains

		validates :bio,length: { maximum: 100 }
		validates :name, presence: true
		validates :age, numericality: {only_integer: true, message: "Solo Numeros Enteros"}
		# Validamos en una expresion regular nuestro email
 	 	VALID_EMAIL_REGEX = /\A[\w+\-.]+@[a-z\d\-.]+\.[a-z]+\z/i
  		validates :email, format: { :with => VALID_EMAIL_REGEX , message: "El formato del correo es invalido" }
  		mount_uploader :avatar, AvatarUploader
end
