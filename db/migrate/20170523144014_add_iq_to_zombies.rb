class AddIqToZombies < ActiveRecord::Migration[5.0]
  def change
    add_column :zombies, :iq, :int
  end
end
